# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from example_app.models import (TodoSerializer, Todo)

from ..keyvalue import get_client
from ..backends import (RedisBackend)

import fakeredis

class GetClientTestCase(TestCase):

    def setUp(self):
        pass

    def test_get_redis(self):
        conn = fakeredis.FakeStrictRedis()
        instance = get_client('drf_keyvalue.backends.RedisBackend', connection=conn)
        assert isinstance(instance, RedisBackend)

    def test_get_requests(self):
        conn = fakeredis.FakeStrictRedis()
        instance = get_client(
            'drf_keyvalue.backends.RequestsBackend',
            base_url="google.com"
        )
        assert instance.base_url == "google.com"

