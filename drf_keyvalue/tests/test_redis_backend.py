# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import fakeredis
from django.test import TestCase
from example_app.models import (TodoSerializer, Todo)
from ..backends import RedisBackend

class RedisBackendTestCase(TestCase):

    def setUp(self):
        self.conn = fakeredis.FakeStrictRedis()
        self.todo = Todo.objects.create(name="cool name", text="do a thing")

    def test_set_value(self):
        collection = RedisBackend(self.conn).collection('todos')
        collection.set(TodoSerializer, self.todo)
        assert self.conn.get('todos.{}'.format(self.todo.pk)) is not None

    def test_get_value(self):
        collection = RedisBackend(self.conn).collection('todos')
        collection.set(TodoSerializer, self.todo)
        result = collection.get(self.todo)

        self.assertEqual(result.get('id'), self.todo.id)

    def test_returns_none_if_no_key_exists(self):
        todo = Todo.objects.create(text="do a thing")
        collection = RedisBackend(self.conn).collection('todos')
        assert collection.get(todo) is None

    def test_delete_value(self):
        collection = RedisBackend(self.conn).collection('todos')
        collection.set(TodoSerializer, self.todo)
        collection.delete(self.todo)
        assert collection.get(self.todo) is None

    def test_set_value_with_specific_key(self):
        collection = RedisBackend(self.conn).collection('todos')
        collection.set(TodoSerializer, self.todo, 'name')
        assert self.conn.get('todos.{}'.format(self.todo.name)) is not None

    def test_get_value_with_specific_key(self):
        collection = RedisBackend(self.conn).collection('todos')
        collection.set(TodoSerializer, self.todo, 'name')
        result = collection.get(self.todo, 'name')

        self.assertEqual(result.get('id'), self.todo.id)

    def test_delete_value_with_specific_key(self):
        collection = RedisBackend(self.conn).collection('todos')
        collection.set(TodoSerializer, self.todo, 'name')
        collection.delete(self.todo, 'name')
        assert collection.get(self.todo) is None


