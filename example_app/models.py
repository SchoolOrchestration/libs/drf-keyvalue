from django.db import models
from rest_framework import serializers

class Todo(models.Model):

    collection = 'todos'

    name = models.CharField(blank=True, null=True, max_length=255)
    text = models.CharField(blank=True, null=True, max_length=255)
    done = models.BooleanField(default=False)

    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = '__all__'
